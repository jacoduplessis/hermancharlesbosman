---
title: Herman Charles Bosman
---

This site is a work in progress.

The goal of this project is to collect
all the works of Herman Charles Bosman 
in one, easily accessible place.

This website is <a target="_blank" href="https://gitlab.com/jacoduplessis/hermancharlesbosman/">open source</a>. 
Contributions welcome. Content retrieved with OCR so there will be errors.