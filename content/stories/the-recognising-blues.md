---
title: The Recognising Blues
source: "Almost Forgotten Stories"
notes: |+

    __Publisher's note in "Almost Forgotten Stories"__
    
    Using Bosman's reference to Cape Town and the typewriter on
    which the manuscript was typed as dating devices, **The Recognising 
    Blues** must have been written after his brief unhappy period as
    the Cape Town representative of that ill-starred publishing venture
    in the winter of 1947.
    
    This is the only story in the whole collection to be drawn from
    the holdings of the Humanities Research Center at Austin, Texas,
    where the Bosman papers are presently lodged. In 1961, while on
    a lecture tourfrom the University of Texas, Professor Joseph Jones
    evinced an interest in Bosman and successfully negotiated for the
    acquisition of his papers on their behalf. Helena Lake, Bosman's
    widow and copyright-holder and Lionel Abrahams, his unofficial
    literary executor, agreed to this arrangement only after an appeal
    through the media failed to yield a local equivalent institution to
    undertake the responsibility of their preservation. To ensure their
    safety they were sent to Texas in batches, the last parcel being
    posted in 1962.
---

I was ambling down Eloff Street, barefooted and in my
shirt-sleeves, and with the recognising blues.

I had been smoking dagga, good dagga, the real rooibaard, 
with heads about a foot long, and not just the stuff
that most dealers supply you with, and that is not much
better than grass. When you smoke good dagga you get
blue in quite a number of ways. The most Common way is
the frightened blues, when you imagine that your heart is
palpitating, and that you can't breathe, and that you are
going to die. Another form that the effect of dagga takes is
that you get the suspicious blues, and then you imagine that
all the people around you, your best friends and your
parents included, are conspiring against you, so that when
your mother asks you, "How are you?" every word she says 
sounds very sinister, as though she knows that you
have been smoking dagga, and that you are blue, and you
feel that she is like a witch. The most innocent remark any
person makes when you have got the suspicious blues seems
to be impregnated with a whole world of underhand mean-
ing and dreadful insinuation.

And perhaps you are right to feel this way about it. Is not
the most harmless conversation between several human
beings charged with the most diabolical kind of subterranean 
cunning, each person fortifying himself behind
barbed-wire defences? Look at that painting of Daumier's,
called Conversation Piece, and you will see that the two
men and the woman concerned in this little friendly chat are
all three of them taking part in a cloven-hoofed rite. You
can see each one has got the suspicious blues.

There is also the once-over blues and a considerable
variety of other kinds of blues. But the recognising blues
doesn't come very often, and then it is only after you have
been smoking the best kind of rooibaard boom, with ears
that long.

When you have got the recognising blues you think you
know everybody you meet. And you go up and shake hands
with every person that you come across, because you think
you recognise him, and you are very glad to have run into
him: in this respect the recognising blues is just the 
opposite of the suspicious blues.

A friend of mine, Charlie, who has smoked dagga for
thirty years, says that he once had the recognising blues
very bad when he was strolling through the centre of the
town. And after he had shaken hands with lots of people
who didn't know him at all, and whom he didn't know
either, but whom he thought he knew, because he had the
recognising blues — then a very singular thing happened to
my friend, Rooker Charlie. For he looked in the display
window of a men's outfitters, and he saw two dummies
standing there, in the window, two dummies dressed in a
smart line of gents' suitings, and with the recognising 
blues strong on him, Charlie thought that he knew those two
dummies, and he thought that the one dummy was Max
Chaitz, who kept a restaurant in Cape Town, and that the
other dummy was a well-known snooker-player called Pat
O'Callaghan.

And my friend Rooker Charlie couldn't understand how
Max Chaitz and Pat O'Callaghan should come to be 
standing there holding animated converse in that shop-window.
He didn't know, until that moment, that Max Chaitz and
Pat O'Callaghan were even acquainted. But the sight of
these two men standing there talking like that shook my
friend Rooker Charlie up pretty badly. So he went home to
bed. But early next morning he dashed round again to that
men's outfitters, and then he saw that those two ﬁgures
weren't Max Chaitz and Pat O'Callaghan at all, but two
dummies stuck in the window. And he saw then that they
didn't look even a bit like the two men he thought they
were — especially the dummy that he thought was Max
Chaitz. Because Max Chaitz is very short and fat, with a
red, cross-looking sort of a face that you can't mistake in a
million. Whereas the dummy was tall and slender and
good-looking.

That was the worst experience that my friend Rooker
Charlie ever had of the recognising blues.

And when I was taking a stroll down Eloff Street, that
evening, and I was barefooted and in my shirt-sleeves, then
I also had a bad attack of the recognising blues. But it was
the recognising blues in a slightly different form. I would
first make up a name in my brain, a name that sounded
good to me, and that I thought had the right sort of a
rhythm to it. And then the first person I would see, I would
think that he was the man Whose name I had just thought
out. And I would go up and address him by this name, and
shake hands with him, and tell him how glad I was to see
him.

And a name I thought up that sounded very ﬁne to me,
and impressive, with just the right kind of ring to it, was the 
name Sir Lionel Ostrich de Frontignac. It was a very
magnificent name.

And so I went up, barefooted and in my shirt-sleeves, to
the first man I saw in the street, after I had coined this name,
and I took him by the hand, and I said, "Well met, Sir
Lionel. It is many years since last we met, Sir Lionel Ostrich
de Frontignac."

And the remarkable coincidence was that the man whom
I addressed in this way actually was Sir Lionel Ostrich de
Frontignac. But on account of his taking me for a bum – 
through my being bare-footed and in my shirt-sleeves – he
wouldn't acknowledge that he really was Sir Lionel and that
I had recognised him dead to rights.

"You are mistaken," Sir Lionel Ostrich de Frontignac
said, moving away from me, "You have got the recognising
blues."