---
title: Great Uncle Joris
source: "Almost Forgotten Stories"
notes: |+

    __Publisher's note in "Almost Forgotten Stories"__

    When Great Uncle Joris appeared in the December issue of Trek
    1948, although Bosman was also contributing in Afrikaans to
    Ruiter and Brandwag, his magazine output had been slender. He
    had poured his creative energy instead into Cold Stone Jug, that
    autobiographical novel he referred to as "A chronicle: being the
    unimpassioned record of a somewhat lengthy sojourn in prison."
    
---

For quite a number of Boers in the Transvaal Bushveld the
expedition against Majaja's tribe of Bechuanas — we called
them the Platkop kafirs — was unlucky.

There was a young man with us on this expedition who
did not finish a story that he started to tell of a bygone war.
And for a good while afterwards the relations were 
considerably strained between the old-established Transvalers living
in these parts and the Cape Boers who had trekked in more
recently.

I can still remember all the activity that went on north of
the Dwarsberge at that time, with veld-kornets going from
one farm-house to another to recruit burghers for the 
expedition, and with provisions and ammunition having to be
got together, and with new stories being told every day
about how cheeky the Platkop kafirs were getting.

I must mention that about that time a number of Boers
from the Cape had trekked into the Marico Bushveld. In the
Drogedal area, indeed, the recently-arrived Cape Boers were
almost as numerous as the Transvalers who had been settled
here for a considerable while. At that time I, too, still
regarded myself as a Cape Boer, since I had only a few years
before quit the Schweizer-Reineke district for the Transvaal.
When the veld-kornet came to my farm on his recruiting
tour, I volunteered my services immediately.

"Of course, we don't want everybody to go on commando," 
the veld-kornet said, studying me somewhat
dubiously, after I had informed him that I was from the
Cape, and that older relatives of mine had taken part in wars
against the kafirs in the Eastern Province. "We need some
burghers to stay behind to help guard the farms. We can't
leave all that to the women and children."

The veld-kornet seemed to have conceived an unreasonable 
prejudice against people whose forebears had fought
against the Xosas in the Eastern Province. But I assured him
that I was very anxious to join, and so in the end he
consented. "A volunteer is, after all, worth more to a
ﬁghting force than a man who has to be commandeered
against his will," the veld-kornet said, stroking his beard.
"Usually."

A week later, on my arrival at the big camp by the
Steenbokspruit, where the expedition against the Platkop
kafirs was being assembled, I was agreeably surprised to
ﬁnd many old friends and acquaintances from the Cape
Colony among the burghers on commando. There were
also a large number of others, whom I then met for the first
time, who were introduced to me as new immigrants from
the Cape.

Indeed, among ourselves we spoke a good deal about this
proud circumstance — about the fact that we Cape Boers
actually outnumbered the Transvalers in this expedition
against Majaja — and we were glad to think that in time of
need we had not failed to come to the help of our new
fatherland. For this reason the coolness that made itself felt
as between Transvaler and Cape Boer, after the expedition
was over, was all the more regrettable.

We remained camped for a good number of days beside
the Steenbokspruit. During that time I became friendly With
Frikkie van Blerk and Jan Bezuidenhout, who were also
originally from the Cape. We craved excitement. And when
we were seated around the camp-ﬁre, talking of life in the
Eastern Province, it was natural enough that we should find
ourselves swopping stories of the adventures of our older
relatives in the wars against the Xosas. We were all three
young, and so we spoke like veterans, forgetting that our
knowledge of frontier fighting was based only on hearsay.
Each of us was an authority on the best way of defeating a
Xosa impi without loss of life to anybody except the
members of the impi. Frikkie van Blerk took the lead in this
kind of talk, and I may say that he was peculiar in his
manner of expressing himself, sometimes. Unfeeling, you
might say. Anyway, as the night wore on, there were in the
whole Transvaal, I am sure, no three young men less
worried than we were about the different kinds of calamities
that, in this uncertain world, could overtake a Xosa impi.

"Are you married, Schalk?" Jan Bezuidenhout asked me,
suddenly.

"No." I replied, "but Frikkie van Blerk is. Why do you
ask?"

Jan Bezuidenhout sighed.

"It is all right for you," he informed me. "But I am also
married. And it is for burghers like Frikkie van Blerk and
myself that a war can become a most serious thing. Who is
looking after your place while you are on commando,
Frikkie?"

Frikkie van Blerk said that a friend and neighbour,
Gideon Kotze, had made special arrangements with the
veld-kornet, whereby he was released from service with the
commando on condition that he kept an eye on the farms
within a 20-mile radius of his own.

"The thought that Gideon Kotze is looking after things, in
that way, makes me feel much happier," Frikkie van Blerk
added. "It is nice for me to know that my wife will not be
quite alone all the time."

"Gideon Kotze —" Jan Bezuidenhout repeated, and sighed
again.

"What do you mean by that sigh?" Frikkie van Blerk
demanded, quickly, a nasty tone seeming to creep into his
voice. "Oh, nothing," Jan Bezuidenhout answered, "oh, nothing
at all."

As he spoke he kicked at a log on the edge of the fire. The
fine sparks rose up very high in the still air and got lost in
the leaves of the thorn-tree overhead.

Frikkie van Blerk cleared his throat. "For that matter," he
said in a meaningful way to Jan Bezuidenhout, "you are also
a married man. Who is looking after your farm — and your
wife — while you are sitting here?"

Jan Bezuidenhout waited for several moments before he
answered.

"Who?" he repeated. "Who? Why, Gideon Kotze — also."

This time when Jan Bezuidenhout sighed, Frikkie van
Blerk joined in audibly. And I, who had nothing at all to do
with any part of this situation, seeing that I was not
married, found myself sighing as well. And this time it was
Frikkie van Blerk who kicked the log by the side of the fire.
The chunk of white wood, which had been hollowed out by
the ants, fell into several pieces, sending up a fiery shower
so high that, to us, looking up to follow their ﬂight, the
yellow sparks became for a few moments almost 
indistinguishable from the stars.

"It's all rotten," Frikkie van Blerk said, taking another
kick at the crumbling log, and missing.

"There's something in the Bible about something else
being something like the sparks ﬂying upwards," Jan 
Bezuidenhout announced. His words sounded very solemn. They
served as an introduction to the following story that he told
us:

"It was during my grandfather's time," Jan Bezuidenhout
said. "My great-uncle Joris, who had a farm near the
Keiskama, had been commandeered to take the field in the
Fifth Kafir War. Before setting out for the war, my 
great-uncle Joris arranged for a friend and neighbour to visit his
farm regularly, in case his wife needed help. Well, as you
know, there is no real danger in a war against kafirs —"

"Yes, we know that," Frikkie van Blerk and I agreed
simultaneously, to sound knowledgeable.

"I mean, there's no danger as long as you don t go so near
that a kafir can reach you with an assegai," Jan 
Bezuidenhout continued. "And, of course, no white man is as 
uneducated as all that. But what happened to my great-uncle Joris
was that his horse threw him. The commando was retreating 
just about then —"

"To reload," Frikkie van Blerk and I both said, eager to
show how well-acquainted we were with the strategy used
in kafir wars.

"Yes," Jan Bezuidenhout went on. "To reload. And there
was no time for the commando to stop for my great-uncle
Joris. The last his comrades saw of him, he was crawling on
his hands and knees towards an aardvark-hole. They did not
know whether the Xosas had seen him. Perhaps the 
commando had to ride back fast because —"

Jan Bezuidenhout did not finish his story. For, just then, a
veld-kornet came with orders from Commandant Pienaar.
We had to put out the fire. We had not to make so much
noise. We were to hold ourselves in readiness, in case the
kafirs launched a night attack. The veld-kornet also 
instructed Jan Bezuidenhout to get his gun and go on guard
duty.

"There was never any nonsense like this in the Cape,"
Frikkie van Blerk grumbled, "when we were fighting the
Xosas. It seems the Transvalers don't know what a kafir
war is."

By this time Frikkie van Blerk had got to believe that he
actually had taken part in the campaigns against the Xosas.

I have mentioned that there were certain differences 
between the Transvalers and the Cape Boers. For one thing,
we from the Cape had a lightness of heart which the
Transvalers lacked — possibly (I thought at the time) because
the stubborn Transvaal soil made the conditions of life more
harsh for them. And the difference between the two sections
was particularly noticeable on the following morning, when
Commandant Pienaar, after having delivered a short speech
about how it was our duty to bring book-learning and
refinement to the Platkop kafirs, gave the order to
advance. We who were from the Cape cheered lustily. The
Transvalers were, as always, subdued. They turned pale,
too, some of them. We rode on for the best part of an hour.
Frikkie van Blerk, Jan Bezuidenhout and I found ourselves
together in a small group on one flank of the commando.

"It's funny," Jan Bezuidenhout said "but I don't see any
kafirs, anywhere, with assegais. It doesn't seem to be like
it was against the Xosas —"

He stopped abruptly. For we heard what sounded 
surprisingly like a shot. Afterwards we heard what sounded 
surprisingly like more shots.

"These Platkop Bechuanas are not like the Cape Xosas," I
agreed, then, dismounting.

In no time the whole commando had dismounted. We
sought cover in dongas and behind rocks from the fire of an
enemy who had concealed himself better than we were
doing.

"No, the Xosas were not at all like this," Frikkie van
Blerk announced, tearing off a strip of shirt to bandage a
place in his leg from which the blood ﬂowed. "Why didn't
the Transvalers let us know it would be like this?"

It was an ambush. Things happened very quickly. It
became only too clear to me why the Transvalers had not
shared in our enthusiasm earlier on, when we had gone over
the rise together, at a canter, through the yellow grass,
singing. I was still reflecting on this circumstance, some
time later, when our commando remounted and galloped
away out of that whole part of the district. To reload, we
said, years afterwards, to strangers who asked. The last we
saw of Jan Bezuidenhout was after he had had his horse shot
down from under him. He was crawling on hands and
knees in the direction of an aardvark-hole.

"Like grand-uncle, like nephew," Frikkie van Blerk said,
when we were discussing the affair some time later, back in
camp beside the Steenbokspruit. Frikkie van Blerk's 
unfeeling sally was not well-received.

Thus ended the expedition against Majaja, that brought
little honour to the commando that took part in it. There
was not a burgher who retained any sort of a happy
memory of the affair. And for a good while afterwards the
relations were strained between Transvaler and Cape Boer
in the Marico.

It was with a sense of bitterness that, some months later, I
had occasion to call to mind that Gideon Kotze, the man
appointed to look after the farms of the burghers on 
commando, was a Transvaler.

And when I saw Gideon Kotze sitting talking to Jan
Bezuidenhout's widow, on the front stoep of their house, I
wondered what the story was, about his grand-uncle Joris,
that Jan Bezuidenhout had not been able to finish telling.
