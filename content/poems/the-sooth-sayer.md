---
title: The Sooth-Sayer
date: 2018-01-27T21:54:35+02:00
tags: []
description: ""
---

"Witch-doctor, throw the bones for me,"
The young man said.
"Black man, what do you see?"

I see you wear a chief's kaross,
And on your head
Is a heavy thing.

"Does that mean evil fare for me?"
It means that you'll be king.

I see the wing of a bush-dove
With a feather broke in twain.

"And does that mean a thing of pain?"
It means that you will love.

I see a dark man, long and lean:
He comes to you; his foot walks slow.

"Witch-doctor, what does that mean?"
I do not know.
