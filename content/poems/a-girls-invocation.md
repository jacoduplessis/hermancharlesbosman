---
title: "A Girls Invocation"
date: 2018-01-27T22:12:08+02:00
tags: []
description: ""
---

Dark stranger, is my house not far?
Why tarriest thou without the gate
When I have bid thee enter in?
See, as thy bride I have loosed my hair; –
What shall I say if thou art late,
The flame gone out, my cheeks grown thin
With watching, and my idle hands
Turned to the morrow's sandy task?
Come while the violet glows in my glands
And on my face is youth's rose mask.
If thou art late – think of my shame
If they should say "She had no charms
To lure him warm to her young arms;
It was in pity that death came."
