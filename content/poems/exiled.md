---
title: "Exiled"
date: 2018-01-27T22:17:30+02:00
tags: []
description: ""
---

One night-hour a shy fern forgot to throw
Her shadow on the ground. I went by fast
In pity for the fern that did not cast,
Like her unsleeping sisters, a shadow,
Moon-maimed. The fairies stood and watched me go
Out of the garden. On the world a vast
Unbroken spell lay in an hour gone past.
They knew so much, and yet they did not know,
For I had gone to find again that brief
Hour when there fell no moon-shade from the fern;
And I would walk on lonely paths in grief,
Speaking of moons to men who could not learn,
Holding out shadows for their unbelief.
