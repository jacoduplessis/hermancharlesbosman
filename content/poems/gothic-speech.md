---
title: "Gothic Speech"
date: 2018-01-27T22:23:41+02:00
tags: []
description: ""
---

Come not too near the fragrance
Lest it break
Anciently.

Between the first hour
After dusk
And the inner folds
Of the drapery about the statue,
I said strange things
That trailed familiarly by lonely
Monuments
In long-forgotten cities.

And is the mystery of your brain
But as a wheel
That turns?

No so ... But as a wheel
That turneth.
