# Herman Charles Bosman

This is a website to showcase the work of one of South Africa's greatest authors.

The website is build with Hugo.

## Scaffold

```
hugo new stories/new-story.md
```

## Using Scan Tailor and Tesseract for text extraction

### Clean images

```
scantailor-cli ./src/* ./dist/
```

### OCR

```
parallel "tesseract {} {} -l eng -psm 4;" ::: ./dist/*.tif
```

or sequentially

```
find ./dist/ -maxdepth 1 -name "*.tif" -exec tesseract '{}' '{}' -psm 4 -l eng \;
```

### Rename

```
rename 's/.tif.txt/.md/' *.txt
```